import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-input/paper-input.js';
import 'web-animations-js/web-animations-next.min.js';

class PageSearch extends PolymerElement {
  static get template() {
    return html`
<style type="text/css">

</style>

<paper-input label="disease search" value="{{filter::change}}"></paper-input>

filter: {{filter}}
`;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      filter: {
        type: String,
        notify: true,
      },
    };
  }
}

customElements.define('wc-page-search', PageSearch);
